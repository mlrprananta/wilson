let connect = require('commandSystem/connect.js');
let Help = require('./help.js');

/**
 * Binds input properties with an aditional value
 * @param {*} props Input properties
 */
function mapProps(props) {
    return Object.assign({}, props);
}

/**
 * Options for the example command
 */
let commandOptions = {
    name: 'help',
    description: 'Shows this help message'
};

/**
 * Connects the options and property mapper
 */
module.exports = connect(mapProps, commandOptions, Help);