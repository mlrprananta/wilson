let Command = require('commandSystem/command');
let Discord = require('discord.js');
let ReactionTracker = require('utility/reactionTracker.js');

/**
 * Help command
 * Lists alll known commands and their details
 */
module.exports = class Help extends Command {

    increasePage(post) {
        this.page = Math.min(this.page + 1, this.max_pages);
        this.update(post);
    }

    decreasePage(post) {
        this.page = Math.max(this.page - 1, 1);
        this.update(post);
    }

    update(post) {
        post.edit(this.createEmbed(this.page));
    }

    createEmbed(page) {
        let maxCharacters = 1000;
        if (isNaN(page)) {
            message.channel.send('Please provide a number');
            return;
        }
        let current_min = (page - 1) * maxCharacters;
        let current_max = page * maxCharacters;
        let embed = new Discord.RichEmbed();
        let description = this.props.responder.getDescription(current_min, current_max, 0);
        this.max_pages = Math.ceil(description.count/maxCharacters);
        if (page > this.max_pages || page < 0) {
            return false;
        }
        let msg = description.msg + `\n page ${page}/${this.max_pages}`;
        embed.setTitle("Commands");
        embed.setDescription(msg);
        return embed;
    }

    /**
     * Command that uses its properties and arguments
     * @param {*} msg Input message
     */
    run(message) {
        this.page = 1;
        let tracker = new ReactionTracker({
            timeout: undefined,
            reactionMap: [
                {symbol:'⬅', onReaction: this.decreasePage.bind(this)},
                {symbol:'➡', onReaction: this.increasePage.bind(this)}
            ]
        });
        let embed = this.createEmbed(this.page);
        message.channel.send(embed).then(post => {
            tracker.bindPost(post);
        });
    }
};