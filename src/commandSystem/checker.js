
/**
 * Runs all known checks for the given state and message
 * Returns with success = true if checks are passed
 * Returns with success = false if checks failed
 * Rerturns with an error if matched but with an exception
 * @param {*} state State to check with
 * @param {*} msg Message to check with
 */
module.exports = (state, msg) => {
    let checks = [
        () => {return {success: state.active};},
        checkBot.bind(state),
        checkName.bind(state),
        runChildChecks.bind(state),
        checkDirect.bind(state),
        checkChannel.bind(state),
        checkUser.bind(state),
        checkVoice.bind(state),
        checkArgs.bind(state)
    ];

    let result = {};
    for (let i = 0; i < checks.length; i ++) {
        let check = checks[i];
        result = Object.assign(result, check(msg));
        if (!result.success) {
            break;
        }
    }
    return result;
};

/**
 * Checks if the author is a bot
 * @param {*} msg The discord msg
 */
function checkBot(msg) {
    return {success: !msg.author.bot};
}

/**
 * Checks if the name matches the command
 * @param {*} msg The discord msg
 */
function checkName(msg) {
    let msgSplit = msg.content.split(/\s+/g);
    command = msgSplit[0].toLowerCase().slice(1);
    return {success: this.name == undefined || command == this.name};
}

/**
 * Checks if the arguments match the requirements
 * @param {*} msg The discord msg
 */
function checkArgs(msg) {
    if (!this.checkParameters) {
        return {success: true};
    }
    let msgSplit = msg.content.split(/\s+/g);
    msgSplit[0].toLowerCase().slice(1);
    let args = msgSplit.slice(1);

    let keys = Object.keys(this.parameters);
    for (let i = 0; i < keys.length; i++) {
        let param = this.parameters[keys[i]];
        if ((args[i] && param.match && !param.match(args[i])) || (!args[i] && param.default == undefined)) {
            let error = "Sorry the provided arguments do not match: `";
            for(let j = 0; j < keys.length; j++) {
                if(j != 0) {
                    error += ', ';
                }
                let req_param = this.parameters[keys[j]];
                error += keys[j];
                if (req_param.default != undefined) {
                    error += ` (default: ${req_param.default})`;
                }
            }
            return {success: false, error: error + '`'};
        }
    }
    return {success: args != undefined};
}

/**
 * Checks if the user is in a voice channel
 * If that is required
 * @param {*} msg The discord msg
 */
function checkVoice(msg) {
    if (this.requireVoice && !msg.member.voiceChannel) {
        return {success: false, error: `You need to be in a voice channel to use this \`${msg.content}\``};
    }
    return {success: true};
}

/**
 * Checks if the msg is posted directly to the bot
 * If that is required
 * @param {*} msg The discord msg
 */
function checkDirect(msg) {
    if (this.requireDirect && msg.channel.type != 'dm') {
        return {success: false, error: `\`${msg.content}\` can only be used by sending a direct msg!`};
    }
    return {success: true};
}

/**
 * Checks if the msg is posted in the correct channel
 * If that is required
 * @param {*} msg The discord msg
 */
function checkChannel(msg) {
    if (this.channels.length > 0 && !(this.requireDirect && msg.channel.type === 'dm') && this.channels.indexOf(msg.channel.name) < 0) {
        if(msg.channel.type == 'dm') {
            return {success: false, error: `\`${msg.content}\` cannot be send directly`};
        }
        let channels = this.channels.map(value => {
            return msg.guild.channels.find(x => x.name == value);
        });
        return {success: false, error: `\`${msg.content}\` only works in ${channels}`};
    }
    return {success: true};
}

/**
 * Checks if the users has the right to use this command
 * If that is specified
 * @param {*} msg The discord msg
 */
function checkUser(msg) {
    if (this.users.length > 0 && !this.users.includes(msg.author.id)) {
        let users = this.users.map(value => {
            return msg.guild.members.get(value);
        });
        return {success: false, error: `\`${msg.content}\` can only be used by ${users}`};
    }
    return {success: true};
}

/**
 * Check all known children
 * Returns true if any of them pass the message check
 * @param {*} msg The discord message
 */
function runChildChecks(msg) {
    if (this.children == undefined) {
        return {success: true};
    }
    let result = {success: false};
    for (let i = 0; i < this.children.length; i++) {
        let child = this.children[i];
        let childResult = module.exports(child.state, msg);
        if (childResult.success == true) {
            return { success: true, child};
        } else if (childResult.error) {
            return { success: false, error: childResult.error};
        }
    }
    return result;
}