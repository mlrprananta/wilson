const Responder = require('commandSystem/responder');
const LegacyCommand = require('commandSystem/legacyCommand');
const connect = require('commandSystem/connect');
const fs = require('fs');

/**
 * Responder that loads legacy commands
 */
module.exports = class LegacyResponder extends Responder {

    /**
     * Loads all .js files in the directory as commands.
     */
    loadChildren() {
        let files = fs.readdirSync(this.state.dir);
        files.map(file => {
            if (file.split(".")[1] == "js" && file != 'index.js') {
                let cmd = require(`${this.state.dir}/${file}`);
                if (cmd.help) {
                    this.constructCommand(cmd);
                }
            }
        });
    }

    defaultMap(props) {
        return Object.assign(props);
    }

    /**
     * Adds a legacy command with some options
     * @param {*} options Options for the command
     */
    addCommand(options) {
        this.state.children.push(connect(this.defaultMap, options, LegacyCommand)(this.props));
    }

    /**
     * Generates a new Command object from a legacy command
     * @param {*} cmd The imported legacy command
     */
    constructCommand(cmd){
       this.addCommand({
            name: cmd.help.command,
            description: cmd.help.description,
            run: cmd.run.bind(this),
            parameters: this.constructParameters(cmd)
        });
    }

    /**
     * Generates parameters with proper optional parameters
     * @param {*} cmd The imported legacy command
     */
    constructParameters(cmd) {
        let parameters = {};
        let largesSet = cmd.help.parameters.reduce( (largest, current) => {
            if (!largest || largest.length < current.length){
                largest = current;
            }
            return largest;
        });
        for (let i = 0; i < largesSet.length; i++) {
            let param = largesSet[i];
            if (cmd.help.required <= i) {
                parameters[param] = {
                    default: false,
                    match: value => {
                        if(value == undefined) return false;
                        return true;
                    }
                };
            } else {
                parameters[param] = {};
            }
        }
        return parameters;
    }
};