const checker = require('./checker.js');

/**
 * Options which can be given to the command
 * These are used while checking the command
 */
let commandOptions = {
    name: 'template', // Command name
    requireVoice: false, // If true then the author needs to be in a voice channel
    requireDirect: false, // If true then the author needs to be in a voice channel
    users: [], // If not empty then the author needs to be one of these users TBD
    channels: [], // If not empty then the message needs to be posted in one of these channels TBD
    active: true, // If false the command can not be called
    checkParameters: true, // If true forces user to provide minimum arguments
    parameters: {}, // The parameters, please use the addParameter function to add new parameters. Adding order matters!!!
    description: 'Someone forgot to provide a description...'
    /**
     * Param shape:
     * name: {default, match} or "name"
     */
};

/**
 * Class Command that can be used to check if it matches the message, arguments, channel, etc. before calling
 */
module.exports = class Command {

    /**
     * Creates a Command object
     * Don't use directly use `commandSystem/connect.js`
     * @param {*} props The props could contain stuff like: the discord client object
     * @param {*} name Name to match the command
     * @param {*} commandFunction The function to execute the command
     */
    constructor(props) {
        this.props = props;
    }

    countLetters(input) {
        let rx = /[a-z]/gi;
        let m = input.match(rx);
        if (m) {
            return m.length;
        }
        return 0;
    }

    getDescription(min, max, count) {
        let msg = '';
        msg += `**!${this.state.name} `;
        for (let name in this.state.parameters) {
            let param = this.state.parameters[name];
            if(param.default == undefined) {
                msg += `<${name}> `;
            } else {
                msg += `<${name} (optional)> `;
            }
        }
        msg += "**\n";
        msg += "\t" + this.state.description + "\n\n";
        count += this.countLetters(msg);
        if (count < min || count > max) {
            return { msg:'', count};
        }
        return { msg, count};
    }

    /**
     * Binds options to state.
     * Don't use directly use `commandSystem/connect.js`
     * @param {*} options Given options
     */
    bindOptions(options) {
        this.state = JSON.parse(JSON.stringify(commandOptions));
        this.state = Object.assign(this.state, options);
    }

    /**
     * Calls the command if it passes all the checks
     * @param {*} message The message 
     */
    call(message) {
        let checkResult = checker(this.state, message);
        if (checkResult.success) {
            let msg = message.content.split(/\s+/g);
            msg[0].toLowerCase().slice(1);
            let args = msg.slice(1);
            args = this.createArguments(args);
            this.run(message, args);
            return checkResult;
        }
        return checkResult;
    }

    /**
     * Place holder function
     * @param {*} message The input message
     * @param {*} args The argument options from the message
     */
    run(message, args) {}

    /**
     * Creates argument object if it matches the parameter requirement
     * Returns undefined if it doesn't match
     * @param {*} args The string array of arguments from the message
     */
    createArguments(args) {
        let result = args;
        let keys = Object.keys(this.state.parameters);
        for (let i = 0; i < keys.length; i++) {
            let param = this.state.parameters[keys[i]];
            if ((args[i] && param.match && !param.match(args[i])) || (!args[i] && param.default == undefined)) {
                return undefined;
            }
            if(args[i]) {
                result[i] = args[i];
            } else {
                result[i] = param.default;
            }
        }
        return result;
    }
};