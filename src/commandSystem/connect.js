
/**
 * Function which can be used to connect properties and options
 * @param {*} mapProps Binds target properties given some properties
 * @param {*} options Options for the target command
 * @param {*} Constructor Constructor of command or responder
 */
module.exports = (mapProps, options, Constructor) => {
    return (props) => {
        let finalProps = mapProps(props);
        let obj = new Constructor(finalProps);
        obj.bindOptions(options);
        return obj;
    };
};