const Command = require('commandSystem/command');

/**
 * On run calls function from the state and passes client from the properties
 */
module.exports = class LegacyCommand extends Command {
    run(message, args) {
        this.state.run(this.props.client, message, args);
    }
};