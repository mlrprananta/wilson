const checker = require('./checker.js');
const fs = require('fs');

/**
 * All possible options which can be given to the responder
 * These are used while checking the responder and loading the children
 */
const responderOptions = {
    loadFromDir: false,
    dir: "",
    children: [],
    requireVoice: false, // If true then the author needs to be in a voice channel
    requireDirect: false, // If true then the author needs to be in a voice channel
    users: [], // If not empty then the author needs to be one of these users TBD
    channels: [], // If not empty then the message needs to be posted in one of these channels TBD
    active: true, // If false the command can not be called
};

/**
 * A Responder contains a collection of children
 * A child that passes the check is called on call(msg)
 */
module.exports = class Responder {

    /**
     * Constructs a Responder
     * During construction the children in the folder are loaded
     * @param {*} props The props containing things like the discord client
     */
    constructor(props) {
        this.props = props;
    }

    getDescription(min, max, count) {
        let msg = '';
        let last = {msg, count};
        for(let i in this.state.children) {
            last = this.state.children[i].getDescription(min, max, last.count);
            msg += last.msg;
        }
        return {msg, count: last.count};
    }

    bindOptions(options) {
        this.state = JSON.parse(JSON.stringify(responderOptions));
        this.state = Object.assign(this.state, options);
        if (this.state.loadFromDir) {
            this.loadChildren();
        }
    }

    /**
     * Calls the first known child that passes all the checks. 
     * @param {*} msg The discord message
     */
    call(msg) {
        let check = checker(this.state, msg);
        if (check.success == true) {
            return check.child.call(msg);
        }
        return check;
    }
    
    /**
     * Loads all folders as children.
     */
    loadChildren() {
        let files = fs.readdirSync(this.state.dir);
        files.map(file => {
            if(file != 'index.js') {
                let path = `${this.state.dir}/${file}`;
                this.addChildFromPath(path);
            }
        });
    }

    addChild(child) {
        this.state.children.push(child);
    }

    addChildFromConstructor(childConst) {
        let child = childConst(this.props);
        this.addChild(child);
    }

    addChildFromPath(path) {
        let childConst = require(path);
        if (childConst) {
            this.addChildFromConstructor(childConst);
        }
    }
};