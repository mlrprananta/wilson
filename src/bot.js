const Discord = require("discord.js");
const DBLoader = require('utility/dbLoader.js');
let db = new DBLoader(require.resolve('./data.json'));
let auth = new DBLoader(require.resolve('./auth.json'));

const client = new Discord.Client();
const Commands = require('commands');
const Help = require('specialCommands/help');
let responder;

/*
Todo: random game pick, check if both has game,
 */

const reply = {
    "marco": "Polo!",
    "geodude": embed("https://i.imgur.com/YdVoIqp.png"),
    "praise be": praiseBe(),
    "wow": embed('https://cdn.discordapp.com/attachments/374267554320875521/441316962623291392/wow.gif'),
    "feelsrainman": embed('https://i.imgur.com/vPD5S87.gif'),
    "coggers": embed('https://cdn.discordapp.com/attachments/367765587260080129/440786237448519682/3x.gif'),
    "oh shit": embed('https://cdn.discordapp.com/attachments/374267554320875521/451121252728307714/FoolishWhirlwindIndianringneckparakeet-size_restricted.gif'),
    "👀": embed('https://cdn.discordapp.com/attachments/374267554320875521/458372649811443723/AcceptableNaughtyHammerkop-size_restricted.gif')
};

function loadCommands() {
    responder = Commands({client, db, auth});
    let help = Help({responder});
    responder.state.children.unshift(help);
}

client.on('ready', (evt) => {
    console.log('connected!');
    loadCommands();
});

client.on('message', (message) => {
    if (message.author.bot) return;
    if (message.content.startsWith(auth.prefix)) {
        let result = responder.call(message);
        if (result.success == false && result.error) {
            message.channel.send(result.error);
        } else if (result.success == false){
            message.channel.send('No command found');
        }
    }

    if (reply[message.content.toLowerCase()]) {
        message.channel.send(reply[message.content.toLowerCase()])
            .then()
            .catch(console.error);
    }
});

client.on("error", (e) => console.error(e));
client.on("warn", (e) => console.warn(e));
//client.on("debug", (e) => console.info(e));




function embed(link) {
    return new Discord.RichEmbed()
        .setColor(3553598)
        .setImage(link);
}

function praiseBe() {
    let embed = new Discord.RichEmbed();
    embed
        .setColor(3447003)
        .setTitle("Me RNGsus")
        .setImage('https://s3.amazonaws.com/files.d20.io/images/33679087/1OxkRToSEBz_UbvRVrGp9A/med.jpg?1495996223971');
    return embed;
}

client.login(auth.token);
