const fs = require('fs');

/**
 * Loads a json file and adds possibility to save/load
 * Commands can change the values and all other commands will see the updated db
 * To save to file call save()
 */
module.exports = class dbLoader {

    /**
     * Creates db object and loads values from the database file
     * @param {*} db_path The path to the database file
     * @param {*} default_values The default values which are used when the file is not yet loaded
     */
    constructor(db_path, default_values = {}) {
        this.db_path = db_path;
        this.default_values = default_values;
        this.load();
    }

    /**
     * Clears db and loads new one from file,
     * only needs to be done at the start. Every db object gets updated when it is  changed
     */
    load() {
        Object.assign(this, this.default_values);
        Object.assign(this, JSON.parse(fs.readFileSync(this.db_path, "utf8")));
    }

    /**
     * Saves everything to the file
     */
    save() {
        fs.writeFile(this.db_path, this.customStringify(this), (err) => {
            if (err) console.error(err);
        });
    }

    /**
     * Stringifies object without circular items
     * Also makes sure that db_path and default_values are not included
     * @param {*} o Object to stringify
     */
    customStringify(o) {
        let cache = [this.default_values];
        let string = JSON.stringify(o, function(key, value) {
            if ((typeof value === 'object' && value !== null) || key == 'db_path') {
                if (cache.indexOf(value) !== -1 || key == 'db_path') {
                    return;
                }
                cache.push(value);
            }
            return value;
        });
        return string;
    }
};