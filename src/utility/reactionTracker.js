
/**
 * The default options given to the reaction tracker
 */
const default_options = {
    postReactions: true, // Set false if you don't want the bot to post the reaction options
    timeout: 60000, // The timelimit of the reaction collecter
    users: [], // The users that are allowed to post a reaction
    reactionMap: [] // The map of symbols and their callback function
    /**
     * {symbol: '', onReaction}
     */
};

/**
 * Tracks user reactions to a post and calls a mapped function
 */
module.exports = class ReactionTracker {
    
    constructor(options) {
        this.options =  Object.assign(Object.assign({}, default_options),options);
    }

    /**
     * Binds the reaction collector to a post
     * @param {*} post 
     */
    bindPost(post) {
        if (this.options.postReactions) {
            this.postReactions(post);
        }
        post.createReactionCollector( this.filter.bind(this), {time: this.options.timeout}).on('collect', (reaction, collector) => {
            for (let i in this.options.reactionMap) {
                let current = this.options.reactionMap[i];
                if (current.symbol == reaction.emoji.name && current.onReaction) {
                    current.onReaction(post, this.lastUser, reaction.emoji.name);
                    break;
                }
            }
            reaction.remove(this.lastUser);
        }).on('end', _ => post.clearReactions());
    }

    /**
     * Filters out reactions that are not in the map or by the wrong user
     * @param {*} reaction 
     * @param {*} user 
     */
    filter(reaction, user) {
        if (user.bot) return false;
        let hasReaction = false;
        for (let i in this.options.reactionMap) {
            let current = this.options.reactionMap[i];
            if (current.symbol == reaction.emoji.name) {
                hasReaction = true;
                break;
            }
        }
        if (hasReaction && this.options.users.length > 0) {
            this.lastUser = user.id;
            return this.options.users.includes(user.id);
        }
        this.lastUser = user.id;
        return hasReaction;
    }

    /**
     * Posts the mapped reactions as an indicator to the users
     * @param {*} post 
     */
    postReactions(post, i = 0) {
        if(i < this.options.reactionMap.length) {
            let reaction = this.options.reactionMap[i];
            i++;
            post.react(reaction.symbol).then(() => this.postReactions(post, i));
        }
    }
};