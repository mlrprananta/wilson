let Command = require('commandSystem/command');

/**
 * Kill command
 * Kills the bot
 */
module.exports = class Kill extends Command {

    /**
     * Kills the bot
     * @param {*} msg Input message
     */
    run(msg) {
        let killMessage = 'Farewell, cruel world!';
        msg.channel.send(killMessage).then(() => {process.exit(0);});
    }
};