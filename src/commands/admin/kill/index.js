let connect = require('commandSystem/connect.js');
let Kill = require('./kill.js');

/**
 * Binds input properties with an aditional value
 * @param {*} props Input properties
 */
function mapProps(props) {
    return Object.assign({}, props);
}

/**
 * Options for the example command
 */
let commandOptions = {
    name: 'kill',
    description: 'Kills the bot'
};

/**
 * Connects the options and property mapper
 */
module.exports = connect(mapProps, commandOptions, Kill);