let connect = require('commandSystem/connect.js');
let Example = require('./weather.js');

/**
 * Binds input properties with an aditional value
 * @param {*} props Input properties
 */
function mapProps(props) {
  return Object.assign({
    somePropValue: "Hi there!"
  }, props);
}

/**
 * Options for the example command
 */
let commandOptions = {
  name: 'weather',
  description: 'Weather command',
  parameters: {
    city: { default: 'Delft' },
    unit: { default: '' }
  }
};

/**
 * Connects the options and property mapper
 */
module.exports = connect(mapProps, commandOptions, Example);
