const Command = require('commandSystem/command');
const Discord = require('discord.js')
const fetch = require('node-fetch');


/**
 * Simple example command
 * This class can also be defined in the index file
 * However this allows for testing and improves readability
 */
module.exports = class WeatherCommand extends Command {

  /**
   * Command that uses its properties and arguments
   * @param {*} msg Input message
   * @param {*} args Input arguments
   */
  run(msg, args) {
    let unit = 'metric'
    const imperial = ['f', 'fahrenheit', 'imperial', 'mph']
    const metric = ['c', 'celsius', 'metric', 'm/s']
    args = args.map(e => e.toLowerCase())
    if (args.filter(e => imperial.includes(e)).length > 0) {
      unit = 'imperial'
    }
    args = args.filter(e => !metric.includes(e) && !imperial.includes(e) && e !== '')
    if (args.length === 0) {
      this.fetchCurrent(msg, this.state.parameters.city.default, unit);
    } else {
      this.fetchCurrent(msg, args.join(" "), unit);
    }
  }

  fetchCurrent(msg, city, unit) {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&units=${unit}&appid=${this.props.auth.weather}`)
      .then(response => {
        return response.json()
      }).then(data => {
        this.embedWeather(msg, data, unit)
        console.log("Success")
      }).catch(error => {
        this.embedError(msg, error)
      })
  }

  embedWeather(msg, data, unit) {
    let units = this.getUnits(unit)
    let embed = new Discord.RichEmbed();
    embed
      .setTitle(`Weather in ${data.name}, ${data.sys.country}`)
      .setDescription(`${data.weather[0].description.charAt(0).toUpperCase() + data.weather[0].description.slice(1)} in ${data.name}, ${data.sys.country}`)
      .setThumbnail(`http://openweathermap.org/img/wn/${data.weather[0].icon}.png`)
      .setFooter(`Powered by OpenWeather`)
      .addField('High', `${data.main.temp_max} ${units[0]}`, true)
      .addField('Low', `${data.main.temp_min} ${units[0]}`, true)
      .addField('Recent', `${data.main.temp} ${units[0]}`, true)
      .addField('Wind Speed', `${data.wind.speed} ${units[1]}`, true)
      .addField('Humidity', `${data.main.humidity}%`, true)
    msg.channel.send(embed)
  }

  embedError(msg, error) {
    console.error(error.message);
    let embed = new Discord.RichEmbed().setTitle("Error");
    embed.setDescription(error.message);
    msg.channel.send(embed);
  }

  getUnits(unit) {
    let units = [
      "°C",
      "m/s"
    ]
    if (unit == "imperial") {
      units = [
        "°F",
        "mph"
      ]
    }
    return units
  }
};

