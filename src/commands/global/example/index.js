let connect = require('commandSystem/connect.js');
let Example = require('./example.js');

/**
 * Binds input properties with an aditional value
 * @param {*} props Input properties
 */
function mapProps(props) {
    return Object.assign({
        somePropValue: "Hi there!"
    }, props);
}

/**
 * Options for the example command
 */
let commandOptions = {
    name: 'example',
    description: 'Example command',
    parameters: {
        first: {},
        second: {default: 0}
    }
};

/**
 * Connects the options and property mapper
 */
module.exports = connect(mapProps, commandOptions, Example);
