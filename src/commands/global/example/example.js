let Command = require('commandSystem/command');

/**
 * Simple example command
 * This class can also be defined in the index file
 * However this allows for testing and improves readability
 */
module.exports = class Example extends Command {

    /**
     * Command that uses its properties and arguments
     * @param {*} msg Input message
     * @param {*} args Input arguments
     */
    run(msg, args) {
        msg.channel.send("This is just an example: " + args + ", my current props are: " + this.props.somePropValue);
        msg.channel.send('I am: ' + this.props.client.user);
    }
};