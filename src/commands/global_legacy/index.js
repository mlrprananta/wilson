let connect = require('commandSystem/connect');
let LegacyResponder = require('commandSystem/legacyResponder');

/**
 * Binds the input props
 */
function mapProps(props) {
    return Object.assign({}, props);
}

/**
 * Normal responder options
 * Loads all commands in this directory
 */
const responderOptions = {
    loadFromDir: true,
    dir: __dirname
};

/**
 * Connects the options and property mapper
 */
module.exports = connect(mapProps, responderOptions, LegacyResponder);