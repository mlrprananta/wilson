const Discord = require("discord.js");
const steam_extended = require("../../utility/steam-web-extended");


module.exports.run = function (client, message, args) {
    let user = message.author;
    let other = message.mentions.users.first();
    let steamid = this.props.db[user.id].steamid;
    let otherid = this.props.db[other.id].steamid;

    steam_extended.getCommonList({
        firstID : steamid, 
        secondID : otherid, 
        tagStringExp : args[1], 
        callback : function sendResult(list, error){
            switch(error){
                case 0:
                    let pick = list[Math.ceil(Math.random() * (list.length - 1))];
                    let embed = new Discord.RichEmbed().setTitle(`${pick.name}`);
                    embed.setColor(`#00adee`);
                    embed.setURL(`http://store.steampowered.com/app/${pick.appid}`);
                    embed.setDescription(`[Launch this game!](steam://rungameid/${pick.appid})`);
                    embed.setThumbnail(`http://media.steampowered.com/steamcommunity/public/images/apps/${pick.appid}/${pick.img_icon_url}.jpg`);
                    embed.setFooter(`${pick.playtime_forever} minutes played`);
                    message.channel.send(embed);
                break;
                case 1:
                    message.channel.send("Lol buy some games bruh @" + user);
                break;
                case 2:
                    message.channel.send("Lol buy some games bruh @" + other);
                break;
                case 3:
                    message.channel.send("Sorry but you don't have anything in common :(");
                break;
                case 4:
                    message.channel.send(`You have no games in common given the tags "${args[1]}"`);
                break;
                case -1:
                    message.channel.send("Could not find the steam account of " + user + " \nUse '!connect <steamid>' to connect your steam account");
                break;
                case -2:
                    message.channel.send("Could not find the steam account of " + other + "\nUse '!connect <steamid>' to connect your steam account");
                break;
                case -3:
                    message.channel.send("Hm I can not parse that expression...");
                break;
            };
        }
    });
};

module.exports.help = {
    name: "Randomcommon",
    command: "randomcommon",
    required: 1,
    optional: 2,
    description: [
        "Pick a random game that you have in common with another user.",
        "Pick a random game that you have in common with another user given a set of tags. \n\tYou can combine tags like: co-op&multiplayer&!(fps|3dperson)"
    ],
    parameters: [
        ["user"],
        ["user", "tags"]
    ]
}