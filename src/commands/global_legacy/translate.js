const Discord = require("discord.js");
const translate = require("google-translate-api");
const ReactionTracker = require('utility/reactionTracker.js');

/**
 * Translation function. I'll add description later because I'm lazy
 *
 * author: Luke Prananta
 * @param client
 * @param message
 * @param args
 */
module.exports.run = function(client, message, args) {

    let author = message.author;
    let channel = message.channel;
    let original = 'auto';
    let target = 'en';
    let targetUser = message.mentions.users.first();
    let collection;
    let limit;
    let index = 0;

    let reactionTracker = new ReactionTracker({
        users: [author.id],
        reactionMap: [
            {symbol:'⬆', onReaction: onPrevious},
            {symbol:'⬇', onReaction: onNext}
        ]
    });

    if (targetUser) {
        if(args[1]) {
            target = args[1];
        }
    } else {
        if(args[0]) {
            target = args[0];
        }
    }

    /**
     * Handles user previous reaction
     * @param {*} post 
     */
    function onPrevious(post) {
        index = index + 1 < limit ? index + 1 : limit;
        update(post);
    }

    /**
     * Handles user next reaction
     * @param {*} post 
     */
    function onNext(post) {
        index = index - 1 > 0 ? index - 1 : 0;
        update(post);
    }

    function initialize(source, post) {
        translate(source.cleanContent, {from: original, to: target}).then(result => {
            if (arguments.length < 2) {
                channel.send(embedTranslation(source, result)).then(post => {
                    reactionTracker.bindPost(post);
                });
            } else {
                post.edit(embedTranslation(source, result));
            }
        }).catch(error => {
            embedError(channel, error);
        });
    }

    function update(post) {
        let source = collection.array()[index];
        initialize(source, post);
    }

    function embedTranslation(source, result) {
        return new Discord.RichEmbed().setColor('#ffcc66')
            .setAuthor(source.author.username, source.author.avatarURL)
            .addField("Source", source.cleanContent)
            .addField("Translation", result.text)
            .setFooter(`From ${result.from.language.iso.toUpperCase()} to ${target.toUpperCase()}`)
            .setTimestamp(source.createdAt);
    }

    channel.fetchMessages({ limit: 30 })
        .then(messages => {
            let filtered = messages.filter(m => m.content)
                .filter(m => !m.content.includes('!translate'))
                .filter(m => !m.author.bot);

            if (targetUser) {
                filtered = filtered.filter(m => m.author.equals(targetUser));
            }

            let source = filtered.first();
            if (!source) {
                embedError(channel, new Error('No message found to translate'));
            } else {
                collection = filtered;
                limit = collection.size - 1;
                initialize(source);
            }
        }).catch(console.error);

    if (channel.permissionsFor(client.user).has("MANAGE_MESSAGES")) {
        message.delete();
    }

};

function embedError(channel, error){
    console.error(error.message);
    let embed = new Discord.RichEmbed().setTitle("Error");
    embed.setDescription(error.message);
    channel.send(embed);
}

module.exports.help = {
    name: "Translate",
    command: "translate",
    required: 0,
    optional: 2,
    description: [
        "Translate last sent message in channel."
    ],
    parameters: [
        [],
        ["original language"],
        ["original language", "translated language"],
    ]
};