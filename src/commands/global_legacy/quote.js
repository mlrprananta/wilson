const Discord = require("discord.js");

module.exports.run = (client, msg, args) => {
    let embed = new Discord.RichEmbed().setColor(`#00adee`);
    let targetUser = msg.mentions.users.first();

    if (targetUser) {
        let target = targetUser.lastMessage;
        if (target) {
            createQuote(embed, target, msg);
        } else {
            createError(embed, msg);
        };
    } else {
        msg.channel.fetchMessage(args[0]).then(target => {
            if (target) {
                createQuote(embed, target, msg);
            }
        }).catch(error => {
            if (error) {
                createError(embed, msg);
            }
        });
    }

    if (msg.channel.permissionsFor(client.user).has("MANAGE_MESSAGES")) {
        msg.delete();
    }
};

function createQuote(embed, target, msg) {
    embed.setAuthor(target.author.username, target.author.avatarURL)
        .setDescription(target.content)
        .setFooter(target.channel.name)
        .setTimestamp(target.createdAt);

    msg.channel.send(embed);
}

function createError(embed, msg) {
    embed.setTitle("Error")
        .setDescription(`Message not found`);
    msg.channel.send(embed);
}

module.exports.help = {
    name: "Quote",
    command: "quote",
    required: 1,
    description: [
        "Quote a message"
    ],
    parameters: [['MessageID/mention']]
};