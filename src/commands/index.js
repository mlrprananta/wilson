let connect = require('commandSystem/connect');
let Responder = require('commandSystem/responder');

/**
 * Binds input properties
 * @param {*} props Input properties
 */
function mapProps(props) {
    return Object.assign({}, props);
}

/**
 * Options for the normal responder
 */
const responderOptions = {
    loadFromDir: true,
    'dir': __dirname
};

/**
 * Connects the options and property mapper
 * after loading all commands add the help command by passing in the children
 */
module.exports = connect(mapProps, responderOptions, Responder);