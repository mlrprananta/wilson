let connect = require('commandSystem/connect');
let Responder = require('commandSystem/responder');

/**
 * Binds input properties
 * @param {*} props Input properties
 */
function mapProps(props) {
    return Object.assign({}, props);
}

/**
 * Options for a responder that only works if the message comes from the playlist channel
 */
const responderOptions = {
    loadFromDir: true,
    dir: __dirname,
    channels: ['playlist'],
    requireVoice: true
};

/**
 * Connects the options and property mapper
 */
module.exports = connect(mapProps, responderOptions, Responder);