//__tests__/automocking.test.js
const ping = require('commands/global/ping.js');

/**
 * Simple test if jest is working
 */
test('Test help', () => {
    expect(ping.help).toMatchSnapshot();
});